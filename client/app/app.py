from flask import Flask, jsonify
import requests

app = Flask(__name__)
app.debug = True


@app.route('/')
def index():
    return 'CLIENT. See the <a href="/data">SERVER Data</a>'


@app.route('/data')
def names():
    remoteApi = "http://server:5000/data"
    response = requests.get(remoteApi).json()
    return jsonify(response)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
