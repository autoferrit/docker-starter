from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return 'SERVER: See <a href="/data">My Data</a>'


@app.route('/data')
def names():
    data = {
        "server" : "backend",
        "names": ["John", "Jacob", "Julie", "Jennifer"]
    }
    return jsonify(data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
